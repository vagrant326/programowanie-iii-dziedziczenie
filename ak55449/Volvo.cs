﻿using System;
using Dziedziczenie;

namespace Lab7
{
    public class Volvo : IPojazd
    {



        public Silnik _Silnik { get; private set; }

        public Volvo(Silnik24D silnik)
        {
            silnik =  _Silnik as Silnik24D;
        }

        public void Jedz() => Jedz(1100);
        

       

        public void Jedz(int dystans)
        {
            for (int i = 0; i < dystans; i++)
            {
                _Silnik.Dzialaj();
            }
        }
    }
}